---
title: "Why this game"
draft: false
date: 2019-03-04T11:12:04+01:00
toc: false
menu: main
weight: 80
---

# why positive social disruption requires this board game ?

* a game to test and design a disruptive social justice system
* fast and safe experiments

# Centralized social justice

{{< figure src="/culottes/img/s10_central_authorities.jpg" caption="Blockchains are not extremely useful to central powers." >}}

Example of the Conseils Départementaux:

* French county governments
* modern Robin Hood
* take money from tax payers
* give money and help to people with disabilities, senior citizens loosing autonomy, poor people, pregnant women, ...

Central authorities:

* public servants
* IT-powered
* heavily regulated
* trusted to be compliant to legal texts

Blockchains can help a little:

* a smart contract to distribute donations to designated people
* transparent and direct giving
* [See last year presentation](https://youtu.be/1Bk4jB4p4rM?t=1585)

# Fully distributed social justice (peer-to-peer)

{{< figure src="/culottes/img/s20_distributed.jpg" caption="Blockchains facilitate consensus in fully peer-to-peer systems." >}}

Cons :

* still very experimental, uncertain, riskier
* less reliable than administration, more fraud
* won't support complex social policies until much later

Pros :

* simpler = no need to trust governments or NGOs
* cheaper = no need for employees and servants
* more convenient = no need for complex forms and long waiting lines, globally available
* personalized and disruptive = implement your own social policy

# A smart contract for justice scales

{{< figure src="/culottes/img/s30_justice_scales.jpg" caption="Justice scales for the common good, to be implemented as smart contracts." >}}

Your own social justice smart contract :

* does this account owner deserve benefits from commons or not ?
* will they really benefit from commons or not ?
* will they contribute to common good or not ?
* will they contribute to the freedom of common goods or not ?
* will they bring more social justice or not ?
* will they reimburse pay-it-forward money ?

# see also [pay-it-forward and free money](http://www.akasig.org/2011/02/14/free-money/)

# leaner design process : easier mechanism design

{{< figure src="/culottes/img/s40_design_jeu_de_l_oie_3.jpg" caption="Practical mechanism design is the foundation for cryptoeconomic games." >}}

* principles of mechanism design
  * game theory identifies optimal player behaviors given a set of rules
  * mechanism design identifies optimal rules given desired behaviors from players
  * social science, not "hard" science, studies real humans, requires experiments
* playing doesn't require a PhD
* _" Whatever is well conceived is clearly said, And the words to say it flow with ease. "_ (Nicolas Boileau, French poet)
  * specify, simplify, verify and clarify rules before coding them

# low cost tester recruitment

{{< figure src="/culottes/img/s50_recrutement.jpg" caption="Let's recruit players of all sorts!" >}}

* securing requires learning
* learning requires testing
* testing requires recruiting testers
* recruiting testers requires attracting and facilitating
* people are familiar with board games, not as frightening than a dapp
* playing a board game has more appeal to most people
* enjoyable experience

# iterate faster

{{< figure src="/culottes/img/s60_iterate_faster.jpg" caption="Iterate so fast your wig won't last" >}}

* simpler, cheaper and more convenient than coding a dapp
* quick discussions, quick feedback from players,
* leaner than code
  * some storytelling
  * a set of rules
  * a static web site
* play anytime, anywhere
* do-it-yourself equipment
  * an egg carton
  * scissors
  * chickpeas
  * pencils
  * a Google spreadsheet

# low cost adoption and marketing

{{< figure src="/culottes/img/s70_marketing.jpg" caption="From useful tests to enjoyable fests" >}}

* the main risk is getting ignored or fogotten, not getting hacked
* who cares about DAOs, smart contracts and dapps beyond geeks, specialists and ultra-early adopters?
* do you address the needs and pain points of real users ?
* do you bring the right incentives for people to adopt your system and change their lives for the better ?
* enjoyable experience incentizes more tests
* returning players are your ambassadors and add some virality to your project

# incentivize unexpected or malicious behaviours

{{< figure src="/culottes/img/s80_malicious.jpg" caption="Unexpected and malicious behaviours are extremely valuable" >}}

* challenging users with your need to identify flaws and vulnerabilities in your design lets you anticipate how malicious users could abuse or exploit your system in undesirable ways,
* observing the strategies of your players (especially returning players) lets you anticipate future users strategies and behaviors
* be prepared, simulate, encourage and analyze attacks
* low cost learning, expose your system to the unexpected
* anticipate problems
  * reality is messy
  * detect edge cases
  * incentize malicious behaviours
  * safety problems, usability problems, marketing problems
  * testing security without risking real money and while having funny

# better target real social value and design priorities accordingly

{{< figure src="/culottes/img/s90_balance_priorites.jpg" caption="Prioritize social impact factors" >}}

* richer feedback than with online tests
  * face-to-face group discussions
  * immediate feedback
  * qualitative insights
* wide spectrum feedback :
  * features
  * software risks
  * software + users risks
  * users perception and feedlings
* perceives added value = social impact
* prioritize social impact factors
* discussing with non geeks empowers your target users by letting them ask unexpected questions and suggest alternative design
* identify real-life needs and build empathy with real users
* post-game discussion let's you ask your target users : would you trust such a system rather than a traditional organization with real money and test the amount of trust your design crreated,

# Conclusion

* Have fun, play with me
* [Contribute to this project]({{< ref "contribute.md" >}}).