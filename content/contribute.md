---
title: "Contribute"
draft: false

menu: "main"
weight: 90
draft: false
---

You can contribute to this game.

# play with me

You can play with me, either IRL near Paris, France or online (via Google Hangout or equivalent). Just ask me by email (sig <at> akasig <dot> org), please suggest 3 date and time slots. Each session requires between 4 and 8 players. We can play in English or in French.

# leave comments

Did you have fun ? Did you spot a bug in the site or in the ruleset ? Do you want to request a new feature ? Do you just want to tell what you think ? Just drop a comment below (you will have to signup/signin at Github.com).

# report issues (translation ?)

You can [report issues at gitlab](https://gitlab.com/siggg/culottes)

# organize your own sessions with the rule books

Try to organize your own session by reading the ruleset. Ask questions as comments below because they will help spot the weaknesses in the explanation of the rules.

# fork it and request merges

You can:

* [fork this game at gitlab](https://gitlab.com/siggg/culottes)
* [fork or comment the Google Spreadsheets at Google Drive](https://docs.google.com/spreadsheets/d/1H4vlePftMYvyuFkRnnJuFOS_B0Pv0L4TUgk9htw0TWE/edit?usp=sharing).
