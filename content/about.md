---
title: "About"
date: 2019-03-04T11:12:04+01:00
menu: "main"
weight: 50

# you can close something for this content if you open it in config.toml.
comment: true
mathjax: false
---

# Introduction

This game is setup in a cliche-version of the French Revolution. The sans-culottes are starving : there is not enough bread for all. The great princess has a Ah-ah moment : "let them eat cake !". So be it.

Revolutionaries want sans-culottes and sans-culottes only to get cake. " Privileged do not deserve cake but the Guillotine ! ", they say. Some privileged are hiding and disguising themselves as sans-culottes. How to discriminate genuine sans-culottes from the privileged ?

But grabbers disagree with revolutionaries. They don't care about sans-culottes. They want to grab cake for themselves and themselves only. Some are sans-culottes grabbers. Others are privileged grabbers. Many pretend they are not grabbers but revolutionaries.

In order to keep things peaceful, the Bastille prison is now used as a store for cakes. The people is watching after the cakes. The Phrygian cap is the traditional hat for sans-culottes. So only wearers of the Phrygian cap will get access to cakes. Do you want a phrygian cap ? You must apply for one at the Revolutionary Tribunal. Anyone is allowed to plead and vote for or against you. If you win your trial, you get a cap and are allowed to get cakes, even if you truly were a privileged disguised as a sans-culottes. If you lose, you get beheaded... but can re-apply anytime. 

Are you a sans-culottes revolutionary ? a privileged revolutionary ? a sans-culottes grabber ? a privileged grabber ?

Who will eat cake ?

# Why this game

Culottes is a board game you can play and have fun with.

Culottes is also a board game with a serious purpose : to simulate test and design a disruptive social justice system, test and optimize its rules before implementing them on the Ethereum blockchain as a decentralized application.

See [more details about our goal and design process]({{< ref "why.md" >}})

# You can contribute

You can contribute to this projet. See [how on the Contribute page]({{< ref "contribute.md" >}}).
