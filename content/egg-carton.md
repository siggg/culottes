---
title: "Egg carton"
date: 2018-12-28T14:54:58+01:00
draft: false
toc: false
---

1. Take an egg carton
  {{< figure src="/culottes/img/egg-carton.jpg" caption="Egg carton to be transformed into justice scales." >}}
2. Remove the eggs and enjoy an *omelette*
3. Using a pair of scissors, remove the carton lid
{{< figure src="/culottes/img/egg_carton_step_02.jpg" caption="The carton lid being removed." >}}
4. Remove the carton flap
{{< figure src="/culottes/img/egg_carton_step_03.jpg" caption="The carton flap being removed." >}}
5. Cut pairs of egg dimples
{{< figure src="/culottes/img/egg_carton_step_04.jpg" caption="Pairs of egg dimples cut from the carton" >}}
6. Here are your **justice scales**
{{< figure src="/culottes/img/egg_carton_step_06.jpg" caption="Dimples turned into scales" >}}
7. Using a permanent marker, write a "S" (for **S**ans-culotte) on one dimple and "P" (for **P**rivileged) on the other dimple so that players can clearly see which scale is for which vote.
{{< figure src="/culottes/img/egg_carton_step_08.jpg" caption="One scale marked S and the other marked P" >}}

{{< figure src="/culottes/img/egg_carton_step_09.jpg" caption="Nuts can also be used as cakes when voting S or P" >}}

If you feel artsy, paint the bottom of the **S**ans-culotte scales in red as in order to symbolize **phrygian caps** when put upside down.

