---
title: "Steps"
date: 2018-12-28T14:54:58+01:00
draft: false
toc: false
---

Print one reminder card per player.

Reminder | 10 steps to play your turn | Your actions
--- | --- | ---
Step 1 | Pick your next citizen | Move your **pike**. *Vive la revolution* ? Do you *pass* ?
Step 2 | Buy cockades | Do you *buy a cockade* ?
Step 3 | Donate cakes to the Bastille | How many, if any ?
Step 4 | Cake-vote | Who do you vote about ? **P** or **S** ? How many **cakes** ? Fill the **Book of Public Safety**. Do you roll a 6 ?
Step 5 | Verdict and rewards | **P** or **S** ? How many loosing **cakes** ? Fill a **verdict sheet**. Strike through the **Book of Public Safety**.
Step 6 | Emigrate | Do you *emigrate* ?
Step 7 | Any citizen left ? | Play your next citizen.
Step 8 | Recruit a new citizen | Do you *recruit* ? Fill a **citizen sheet**
Step 9 | Distribute Bastille cakes | Who has a **phrygian cap** ? Each gets a Bastille **cake**.
Step 10 | End of turn | Next player !
