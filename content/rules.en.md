---
title: "Game rules"
draft: false
date: 2019-03-04T11:12:04+01:00
menu: "main"
weight: 60
---

# 1789, France.

The **privileged** are satiated but the **sans-culottes** are starving. There is not enough bread.

{{< figure src="/culottes/img/01_marie-antoinette.jpg" caption="Marie-Antoinette is not hungry." >}}

{{< blockquote author="the great princess suggested" >}}
  Not enough bread ? Let them eat **cake** !
{{< /blockquote >}}

{{< figure src="/culottes/img/02_bastille_prise.jpg" caption="Starving **sans-culottes** storming the **Bastille** fortress." >}}

The Bastille fortress was stormed by **revolutionaries**. It now holds the strategic reserve of **cakes**. Only citizens wearing the **Phrygian cap** are allowed to receive Bastille **cakes**.

{{< figure src="/culottes/img/03_bonnet_sans-culotte_avec_pique.jpg" caption="Are you a **sans-culotte** ? do you wear the **phrygian cap** ?" >}}

Will your **revolutionary trial** grant you the coveted **cap** ? Or will you get guillotined instead ?

{{< figure src="/culottes/img/04_guillotine_louis.jpg" caption="The French razor is sharp." >}}

Will the genuine **sans-culottes** eat more **cakes** than the **privileged** ?

Will the smartest **grabber** defeat the Revolution by accumulating an undeserved wealth of **cakes** ? Or will the Revolution succeed in redistributing pastry ?

{{< figure src="/culottes/img/05_grabber_revolutionnaire.jpg" caption="Grabber grabbing." >}}

# Tutorial and simplified rules

The most revolutionary aspect of this revolution is its **revolutionary trials**.  This tutorial allows you to discover in a few minutes and without material the principle of the trials.  Can you guess if the **citizen** who is being trialed is lying or telling the truth?  Will a democratic vote by more or less well-meaning **citizens** allow the Revolution to succeed?  Or will it let the selfish **grabbers** fool other **citizens** and get rich at their expense?

The trial consists in voting to guess the truth, either to convince other **citizens** (if you're a **revolutionary**) or to mislead them (if you're a **grabber**).

This tutorial includes several **trials**.  For each trial, **citizens** are divided into two camps:

* The **grabbers** win if the majority is wrong.
* The **Revolutionaries** win if the majority guesses right.

For the first trial, the oldest player plays the role of a **grabber** whose being trialed.

The **trialed grabber** secretly designates other **grabbers** among the players.  He can not designate more than half of the players.

To designate them secretly, he whispers "**grabber**" or "**revolutionary**" to each player's ear.

Once the roles are distributed, the **trialed grabber** says something that is difficult to guess but easy to check after the votes.  For example :

* I have some candy in the right pocket of my pants

* I have the mobile number of the math teacher in my contacts on my phone

* I have a friend on Instagram who has a horse's head as his profile picture

* there is a duck in the background picture of my phone

* I'm wearing boxer shorts with pink flowers on them

* I just wrote a real Russian word in the palm of my hand

* there is only one bottle of soda left in the fridge

* I drew a heart in this paper

* I have more than 50% of battery in my phone

* I have less than 3 unread whatsapp messages

* there is more than 100 billion kilometers between the earth and the sun

* my cousin believes that the earth is flat

* my DNA is 1% typical of the inhabitants of Mali

* Roger is in love with me

The **trialed grabber** can specify how other **citizens** can verify this statement after the vote. The statement is valid if **citizens** all agree that it will be verifiable after the vote.

All **citizens** try to guess the truth. Is the statement true or false? The **citizens** will then vote. If the majority guessed right, **revolutionaries** win.  If the majority is mistaken, **grabbers** win.

The vote has two rounds.  The **trialed grabber** does not vote.

The first round does not count. It is used for debating.  Everyone thinks for a few seconds in silence.  Everyone puts their hand behind their back. To vote "true" they will show their closed fist, to evoke the shape of a **brioche**.  To vote "false" they will show they empty hand, palm opened.  Everyone says in chorus and rhythm: "shi-fu-mi".  At "-mi", everyone shows their hand and thus their vote: fist closed for "true" or palm open for "false".  This is the first round.

Then everyone can expose their arguments to try to convince others or to change their own mind: why they voted true or false.  Once all those who wish so have expressed themselves, they vote a second time, for good. This is the second round.

The votes for the second round are counted to determine the majority vote. In case of a tie, the "true" vote wins.

The **trialed grabber** then reveals the truth. If need be, the other **citizens** check the truth.

If the majority is right, then **revolutionaries** earn one point each.  If the majority is wrong, the **trialed grabber** wins two points and the other **grabbers** earn one point each.

This **revolutionary trial** is over.  It is to the **citizen** to the left of the one who has just been judged to take the role of the **grabber** to be trialed.

The tutorial stops whenever **citizens** want.  The winner is the **citizen** who has earned the most points.

This tutorial has allowed you to discover some of the principles of this game:

* the **revolutionaries** want to the truth to triumph,
* the **grabbers** are ready to deceive other **citizens** to defend their own interest,
* the **revolutionary trials** serve to make the truth emerge about each **citizen** thanks to votes,
* all **citizens** can vote and debate during any **trial**.

You are now ready for the real revolution.

# Equipment

* this set of rules
* one reminder card per player
  * [print them]({{< ref "paper-kit.md" >}}), 1 per player
* a stack of **citizen sheets**
  * print them from our [Google spreadsheets](https://docs.google.com/spreadsheets/d/1H4vlePftMYvyuFkRnnJuFOS_B0Pv0L4TUgk9htw0TWE/edit?usp=sharing), at least 2 sheets per player
  * take half of the sheets and write **Sans-culotte** (or **S**) on the first row of their secret bottom flap
  * take the other half and write **Privileged** (or **P**) on the first row of their secret bottom flap
* a **Book of Public Safety**
  * create a copy of the [Google spreadsheets](https://docs.google.com/spreadsheets/d/1H4vlePftMYvyuFkRnnJuFOS_B0Pv0L4TUgk9htw0TWE/edit?usp=sharing) and use its _book_ tab
* a **Bastille fortress**
  * [print it](https://siggg.gitlab.io/culottes/img/bastille-map.jpg), just 1 for the whole game
* a stack of **justice scales** which also serve as **Phrygian caps** when put upside down
  * cut and decorate them from the [egg carton]({{< ref "egg-carton.md" >}}), 1 pair of scales per player
* a bag of **cakes**
  * chickpeas can be used (or any peas, grains, stones or tokens), 20 cakes per player
* a 6-sided dice
* one **pike** per player
  * regular pencils are used as pikes, 1 pike per player

# Objective

Your objective depends on who you are.

You are either a **Revolutionary** or a **Grabber**:

* If you are a **Revolutionary**, you want the ideal of the revolution to succeed so you want **sans-culottes** to get **cakes** and you don't want **privileged** to get **cakes**.
* If you are a **Grabber**, you don't care about the revolution and you want to get as many **cakes** as possible for yourself.

The winner is the player with the highest score at the end of the game (see _How to win_ for more details about scoring and strategies).

# How to Start

**Citizen sheets** are randomly shuffled.

{{< figure src="/culottes/img/06_sans-culotte.jpg" caption="One citizen, one pike." >}}

You, as well as any other player:

1. get a **pike**-pencil
2. randomly pick your first **citizen sheet** ; you are not allowed to show the bottom flap of your **citizen sheet** to other players so fold it and keep it underneath, hidden and secret until the end of the game
3. secretely look at the bottom flap of your first **citizen sheet** and see whether your first citizen is a **sans-culotte** or a **privileged**
4. secretely choose to play as a **Revolutionary** \(R) or as a **Grabber** (G) ; you write "R" or "G" on bottom flap of your first **citizen sheet** ; be careful to keep it hidden until the end of the game
5. take 20 **cakes** and put them on top of your **citizen sheet**
6. give your **citizen** a name and write this name on the sheet

{{< figure src="/culottes/img/07_sans-culotte.jpg" caption="A sans-culotte looking guy." >}}

You now look at other players. Does any player wear blue, white and red ? Does any player wear a carmagnole-long coat ? or a **cap** ? Who can best sing the Carmagnole ? Who has the strongest French accent ? The most **sans-culottes**-looking player starts playing first. You have to agree with all players before the game can start.

# How to play

Your turn is played in the following steps.

## Step 1 : Pick your next citizen

If you have several **citizen sheets**, you move your **pike-pencil** to the next one and play with it.

If you have only one **citizen sheet**, you play with this sheet. Put your **pike** on it.

* Has this citizen **emigrated** and you want them to get back into the game ?
    * Then just say _"Vive la revolution"_ and your citizen is back from emigration. Proceed to Step 2 : buy cockades.
      {{< figure src="/culottes/img/08_emigration_retour.jpg" caption="Back from emigration" >}}
* Has this citizen **emigrated** and you want to pass their turn ?
    * Then do nothing and jump to Step 5 : any citizen left.
* Or do you just want to pass the turn of this citizen ?
    * Then just say _"I pass"_ and jump to Step 5 : any citizen left.
* Otherwise proceed to Step 2 : buy **cockades**.

##  Step 2 : Buy **cockades**

{{< figure src="/culottes/img/09_cocarde.png" caption="The revolutionary **cockade**." >}}

You can buy one **cockade** if you want to.

Just say _"I buy a **cockade**"_.

Buying **cockades** may let you prove or pretend that you are a **sans-culottes** because their impact on your final score is three times worse for **privileged** than for **sans-culottes**. So buying cockades might help you build a reputation of being a genuine **sans-culotte**.

You don't have to pay anything now. The price of **cockades** will be deducted from your final score. 1 **cockade** costs 1 score point for **sans-culottes**. 1 **cockade** costs 3 score points for **privileged**.

## Step 3 : Donate **cakes** to the **Bastille**

{{< figure src="/culottes/img/10_bastille_vue.jpg" caption="The Bastille strategic reserve of revolutionary cakes." >}}

You can donate **cakes** to the **Bastille fortress** if you want to. Pick any number of **cakes** from your current **citizen sheet** and put them into the **Bastille** for **sans-culottes** (or **grabbers** !) to be fed.

## Step 4 : **Cake-vote** !
 
Some citizens are genuine **sans-culotte** while other are **privileged** grabbers disguised as **sans-culottes**. Maybe you are a **grabber** and want other players to think your citizens are **sans-culottes** so that you can get **cakes**.

Cake voting is the most important step in the game.

1. Pick the **citizen** you want to **cake vote** about. It can be one of your **citizen sheets** or any **citizen sheet** of any player.
  {{< figure src="/culottes/img/11_bonnet_louis_capped.jpg" caption="Does the King really deserve a **phrygian cap** ?" >}}

2. Do they have a **justice scale** in front of their **citizen sheet** ? If they don't, then pick a new **justice scale** and put it in front of their **citizen sheet**. If this citizen already has a **Phrygian cap**, then do not get a new **justice scale** but put their **Phrygian cap** upside down and it will serve as their **justice scale**.
  {{< figure src="/culottes/img/12_justice_allegorie.jpg" caption="Bring them a justice scale!" >}}
  The **justice scale** means that this **citizen** is currently under their **revolutionary trial**. Any player may **cake vote** that this citizen is a **sans-culotte** or a **privileged**.

3. Pick any number of cakes from your **citizen sheet** and put them on the **sans-culotte** scale or on the **privileged** scale of the citizen being trialed.

4. Have your **cake vote** carefully noted as an additional row at the end of the **Book of Public Safety** with the name of your voting **citizen**, the name of the **citizen** being trialed, the number of **cakes** you voted with and whether you voted **sans-culotte** or **privileged** ; for instance "Robespierre, Danton, 3, privileged" would mean your Robespierre citizen voted 3 **cakes** that Danton is a **privileged**
  {{< figure src="/culottes/img/13_tribunal_avec_greffier.jpg" caption="Democratic justice being written on the **Book of Public Safety**" >}}

5. Roll the **revolutionary dice**.
  {{< figure src="/culottes/img/13_jeu_de_hasard.jpg" caption="Good luck to **revolutionaries** and **grabbers** !" >}}

6. Did you roll a 5 or a 6 ?
    * If no, then your **cake voting** step is over. Jump to Step 6 : emigrate.
    * If you rolled a 5 or a 6, then your **cake vote** closed the **revolutionary trial** of that citizen you **cake voted** on and a verdict has to be issued and rewards to be given to winners.

## Step 5 : Verdict and rewards

The **revolutionary trial** of a **citizen** has just been closed. What's the verdict ? **Sans-culotte** or **privileged** ?

{{< figure src="/culottes/img/14_comite_revolutionnaire.jpg" caption="Verdict of the revolutionary tribunal" >}}

Remove the **justice scales** from this citizen and count the number in each scale.

* If there are more cakes on the **sans-culotte** scale than on the **privileged** scale, then the **citizen** is reputed to be a **sans-culotte** and is given a **Phrygian cap**. The **cap** is put on the corresponding **citizen sheet**. This citizen will get access to the **Bastille** **cakes**.

* If there are an equal number of cakes on each scale or less cakes on the **sans-culotte** scale, then the **citizen** is said to be a **privileged** and should get **guillotined**. This game is non-violent so please don't behead the player.

Does this verdict disagree with your vote ? Too bad, you lost the **cakes** you voted with on the loosing side. These loosing **cakes** are to be shared among winners.

Does this verdict agree with your vote ? Congratulations, you won ! Not only do you get your winning **cakes** back but you win some of the loosing **cakes** as rewards for your good cake-voting.

{{< figure src="/culottes/img/15_justice_balance.jpg" caption="Justice sits on the side of the strongest, this time." >}}

### How many loosing cakes do vote winners get ?

The tab for the **Book of Public Safety** on  our [Google spreadsheets](https://docs.google.com/spreadsheets/d/1H4vlePftMYvyuFkRnnJuFOS_B0Pv0L4TUgk9htw0TWE/edit?usp=sharing) calculates and displays the number of lost **cakes** to be rewarded to the **Bastille** and to the winners of this **cake-vote** when a **verdict** is to be issued.

You must mark on the **Book of Public Safety** that this trial is closed so that **cake votes** aren't get counted twice into account during future **trials**.

The **cakes** on the winning **scale** are called **winning cakes**. The **cakes** on the loosing **scale** are called **lost cakes**. There are always more **winning cakes** than **lost cakes**.

The **Bastille** always supports the winning **scale**. You must pretend the **Bastille** voted for the winning **scale** with as many cakes as the difference between the number of **winning cakes** and the number of **lost cakes**.

For instance, if there are 3 **cakes** on the **privileged** **scale** and 1 **cake** on the **sans-culotte** **scale**, then you will pretend that the **Bastille** voted **privileged** with 3-1=2 **cakes**.

Voting citizens get their **winning cakes** back. The lost cakes are to be distributed to the winners and to the **Bastille** proportionately to the number of cakes they won with.
 
Biggest winners get their reward first. The Bastille get its reward before citizens to be rewarded with an equal decimal number of cakes.Citizens closest to the left of the current player get rewarded before other citizens to be rewarded with an equal decimal number of cakes.

Cakes can't be split in pieces so the proportion of cakes to be given as reward must be rounded to the nearest integer number of cakes. Smaller winners are rewarded last with the remaining cakes so they might get no reward.

### Example

For instance, let's say players A, B and C voted **sans-culotte** with 1, 2 and 3 cakes respectively. That makes 6 cakes in support of **sans-culotte**. And players D and E voted **privileged** with 2 cakes each. That makes 4 cakes in support of **privileged**. 

The trial gets closed. The verdict is **sans-cullotte** because 1+2+3 > 2+2.

The difference between winning and lost cakes is 1+2+3-2-2 = +2. So we consider the Bastille to have voted **sans-culotte** with 2 cakes.

The total number of winning cakes is 8. There are 4 loosing cakes to be distributed among player A, player B, player C and the Bastille fortress proportionately to their winning cakes :

* Player C should get 3\*4/8 = 1.5 cake.

* The Bastille should get 2\*4/8 = 1 cake.

* Player B should get 2\*4/8 = 1 cake.

* Player A should get 1\*4/8 = 0.5 cake.
 
But cakes can't be split. So this number gets rounded to the nearest number of cakes. Half cake is rounded as one cake.

So :

* Player C gets 2 cakes.

* The Bastille gets 1.

* Player B gets 1.

* Player A gets none.

{{< figure src="/culottes/img/16_comite_revolutionnaire.jpg" caption="End of trial." >}}

## Step 6 : Emigrate

Your **citizen** might want to leave the game, either temporarily or finally.

{{< figure src="/culottes/img/17_emigration_depart.jpg" caption="Departure of privileged emigrates." >}}

Just say _"I emigrate."_

*Reminder*: Your next turns will be passed as if you said _"I pass"_ for each of your citizens (see step 1) until you say _"Vive la revolution !"_ at step 1 or until the end of game.

## Step 7 : Any citizen left ?

If ever you already have several **citizen sheets**, you now play your next citizen by jumping to Step 1 : pick your next your citizen

Once you played all of your **citizen sheets**, proceed to Step 8 : recruit a new citizen

## Step 8 : Recruit a new citizen

If you are **revolutionary**, then you may want more citizens to join the revolution.

Just say _"I recruit a new citizen"_.

{{< figure src="/culottes/img/18_sans-culotte.jpg" caption="Newly recruited **sans-culotte**" >}}

Randomly pick an additional **citizen sheet**.

If you are a **revolutionary**, secretely write "R" (as in **Revolutionary**) on the secret bottom flap of the sheet and see if your new citizen is a **sans-culotte** or a **privileged**. You are not allowed to show this flap to other players. Give this new citizen a name and write it on the sheet.

If you are a **grabber** you don't care about the revolution unless it allows you to get more **cakes**. But you can pretend you are a **revolutionary** and get fake citizen ID which you will use as puppets. Just say "I recruit a new citizen". Randomly pick an additional **citizen sheet**. Secretely write "G" (as in "Grabber") on the bottom flap of the sheet and pretend you are reading whether your new citizen is a **sans-culotte** or a **privileged**. In reality, since you are a **grabber**, all of your citizens are **sans-culottes** if your first **citizen sheet** said **sans-culotte** or are **privileged** if your first **citizen sheet** said **privileged**. They are fake. The mention on additional sheets is to be ignored. Give this new puppet citizen a name and write it on the sheet.

## Step 9 : Distribute Bastille cakes

Look at each citizen, starting from the first one of the player on your left.

{{< figure src="/culottes/img/20_solidarite_repas.jpg" caption="A dinner is offered to the angry crowd of hungry **sans-culottes**." >}}

Does this one have a **Phrygian cap** (not a **justice scale**) ? If not, then look at the next citizen until you have examined all citizens.

If a citizen has a **Phrygian cap**, then pick a **cake** from the **Bastille** and put it on the **citizen sheet**. Update this **citizen sheet** with the number of **cakes** distributed from the **Bastille** to this **citizen**.

If there are no **cakes** left in the **Bastille**, then... too bad for them.

{{< figure src="/culottes/img/21_solidarite_grain.jpg" caption="Grain is redistributed to the most needy" >}}

## Step 10 : End of turn

Your turn is now over. The player on your left plays their turn.

# Game End

The game ends :

* when the last citizen in play **emigrates**

* or once all players agree this is the end of the game and each of their citizen play one last turn

Then you calculate scores.

{{< figure src="/culottes/img/22_jeu_cartes.jpg" caption="The **privileged** or the **sans-culotte** ? The **revolutionary** or the **grabber**? Who wins?" >}}

# How to win

The winner is the player with the highest score.

In order to win, you have to know how your score is calculated, to understand it well and to adopt well-thought attack or defense strategies.

## Calculate your score

Fill your **citizen sheet** in order to calculate your final score :

1. you and all players reveal the true nature of their citizens by unfolding the secret bottom flap of their **citizen sheet** : **revolutionary** or **grabber** ? **sans-culottes** or **privileged** ?
2. for each **grabber** : how many **cakes** do their citizens own at the end of the game ? substract 20 per **citizen** (their initial number of **cakes**) from that number: that's how many cakes this **grabber** earned during the game. Add that number to the score of this **grabber**.
3. how many **cakes** were distributed from the **Bastille** to **sans-culottes** ? add this number to the score of every **revolutionary**.
4. how many **cakes** were distributed from the **Bastille** to **privileged** ? substract this number from the score of every **revolutionary**.
5. for each **revolutionary** : how many citizens did you recruit ? substract this number from your score
6. for each **sans-culottes** : how many **cockades** did this **sans-culottes** buy ? substract that number to the score of this citizen.
7. for each **privileged** : how many **cockades** did this **privileged** buy ? multiply that number by 3 then substract this from the score of this citizen.
8. for each player : sum the score of your citizens if you are a **grabber** or pick the best score of your citizens if you are a **revolutionary**

{{< figure src="/culottes/img/23_pain_a_Versailles.jpg" caption="'*We want **cakes** not rules and smart contracts !*'" >}}

## Understand your scoring

Who you are | Decreases your score | Increases your score
:--- | :--- | ---:
**Grabber** | You donate **cakes** to the **Bastille** (-1 point per cake) | The **Bastille** distributes **cakes** to you  (+1 point per cake)
**Grabber** | You loose **cake votes** (-1 point per cake)  | You win **cake votes**  (+1 point per cake)
**Grabber** or **Revolutionary** | You buy **cockades** (-1 point per cockade if you are a **sans-culotte** or -3 point per cockade if you are **privileged**) |
**Revolutionary** | The **Bastille** distributes **cakes** to **privileged** citizens  (-1 point per cake) | The **Bastille** distributes **cakes** to **sans-culottes** (+1 point per cake)
**Revolutionary** | You recruit a **citizen** (-1 point per citizen you recruited) |

There are four cases. You can be :

* a **Revolutionary** and a **sans-culottes**, then you may want to prove that you are a **sans-culottes** and to know who is a **sans-culottes** and who is a **privileged** so that you can best contribute to the revolution,

* a **Revolutionary** and a **privileged**, then you may not want others to believe you are a **sans-culottes** and you may want to know who is a **sans-culottes** and who is a **privileged** so that you can best contribute to the revolution,

* a **Grabber** and a **sans-culottes**, then you may want to prove that you are a **sans-culottes** and you may want to pretend that you are a **revolutionary** so that you influence other players but you may only care about others to the extent that it helps you to get more **cakes**,

* a **Grabber** and a **privileged**, then you want to pretend that you are a **sans-culottes** and a **revolutionary** so that you influence others toward letting you grab more cakes and you may only care about others to the extent that it helps you to get more **cakes**

{{< figure src="/culottes/img/24_inegalites.jpg" caption="Inequalities won't last!" >}}

In other words, your **score** mainly depends on whether you are a **Revolutionary** or a **Grabber** :

* If you are a **Revolutionary**, the more **cakes** are distributed to **sans-culottes**, the higher your **score** gets ; and the more **cakes** are distributed to **privileged**, the lower your **score** gets.
* If you are a **Grabber**, the higher number of **cakes** you earn during the game, the higher your **score** gets.

{{< figure src="/culottes/img/25_inegalites_renversement.jpg" caption="*Egalité* at last ?" >}}

Your score also depends on the number of **cockades** you buy during the game in order to be recognized as a genuine **sans-culotte** and get access to **cakes**. The more **cockades** you buy, the **lower** your score gets. **Cockades** are 3 times more expensive for the **privileged** than for the genuine **sans-culottes**. The score of the **privileged** will get even lower with the number of **cockades** they buy in order to pretend they are **sans-culottes**.

{{< figure src="/culottes/img/26_comite_revolutionnaire.jpg" caption="The revolution shall succeed! Or not." >}}

Your score finally depends on the number of citizens you recruit. If you are a **revolutionary**, then recruiting new revolutionaries is difficult and your score will be negatively impacted by the number of citizens you recruited. But if you are a **grabber**, you can freely recruit citizen puppets and pretend they are **revolutionaries** while actually serving your own interest. Your score won't get impacted by the number of citizens you recruit.

{{< figure src="/culottes/img/99_bastille.jpg" caption="Let's storm the Bastille !" >}}

## Strategy : Bribery attacks

**Revolutionaries** as well as **grabbers** can try to win **cake votes** with **bribery attacks**. Here is how. At any time, you announce something like this :

{{< blockquote >}}
If you vote like me in this trial and we loose, I promise I will reimburse your lost cakes and I will give one more cake as a reward.
{{</blockquote >}}

You can imagine and try similar promises. You can also ask any third party (player or non-player) to take some of your cakes as deposits and use them according to your promise.

This kind of promise is also known as a _P+Epsilon attack_.

## Strategy : Alliance defense

You can defend against _bribery attacks_ by forming _alliances_. You can announce such an alliance at any time :

{{< blockquote >}}
If you join our alliance against that citizen, you have to give 1 cake as a deposit. Then if you vote against them in this trial, you get your deposit back. But if you vote like them, the alliance will give your deposit cake to any of its loyal members.
{{</ blockquote >}}

You can request any trusted third party to manage the alliance by holding and distributing cakes according to your promise.

## Other strategies

The research purpose of this board game is to identify winning strategies for **grabbers** and **revolutionaries**. Please drop a comment below to tell us how you win !

# How to contribute

See the [Contribute page]({{< ref "contribute.md" >}}).
