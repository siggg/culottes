---
title: "Règles du jeu"
date: 2019-03-04T11:12:04+01:00
draft: false
---

# 1789, France.

Les **privilégiés** sont rassasiés mais les **sans-culottes** meurent de faim. Il n'y a pas assez de pain.

{{< figure src="/culottes/img/01_marie-antoinette.jpg" caption="Burp. Marie-Antoinette n'a plus faim." >}}

{{< blockquote author="suggéra une grande princesse" >}}
  Pas assez de pain ? Qu'ils mangent de la **brioche** !
{{< /blockquote >}}

{{< figure src="/culottes/img/02_bastille_prise.jpg" caption="Les **sans-culottes** affamés prennent la **Bastille**." >}}

La forteresse de la Bastille a été prise d'assaut par les **révolutionnaires**. Ils détiennent maintenant la réserve stratégique de **brioches**. Seuls les citoyens portant le **bonnet phrygien** sont autorisés à recevoir des **brioches** de la Bastille.

{{< figure src="/culottes/img/03_bonnet_sans-culotte_avec_pique.jpg" caption="Etes-vous un **sans-culotte** ? Portez-vous le **bonnet phrygien** ?" >}}

Votre **procès révolutionnaire** vous accordera-t-il le **bonnet** convoité ? Ou allez-vous vous faire guillotiner ?

{{< figure src="/culottes/img/04_guillotine_louis.jpg" caption="Le Rasoir Français est tranchant." >}}

Les véritables **sans-culottes** mangeront-ils plus de **brioches** que les **privilégiés** ?

L' **accapareur** le plus malin va-t-il anéantir la Révolution en accumulant une richesse imméritée de **brioches** ? Ou bien la Révolution parviendra-t-elle à Redistribuer les Pâtisseries ?

{{< figure src="/culottes/img/05_grabber_revolutionnaire.jpg" caption="Accapareur en train d'accaparer." >}}

# Tutoriel et règles simplifiées 

Le plus révolutionnaire, dans cette révolution, c'est le **procès révolutionnaire**. Ce tutoriel vous permet de découvrir en quelques minutes et sans matériel le principe des procès. Saurez-vous deviner si le **citoyen** dont c'est le procès ment ou dit la vérité ? Un vote démocratique effectué par des citoyens plus ou moins bien intentionnés permettra-t-il à la Révolution de réussir ? Ou aux **accapareurs** égoïstes de rouler le peuple dans la farine à brioches et de s'enrichir aux dépends de l'intérêt général ?

Le procès consiste à voter pour deviner la vérité, soit pour en convaincre les autres joueurs (si vous êtes un **révolutionnaire**) soit pour les induire en erreur (si vous êtes un **accapareur**).

Ce tutoriel se déroule en plusieurs procès. À chaque procès, les citoyens sont répartis en deux camps :

* Les **accapareurs** gagnent si la majorité se trompe.
* Les **révolutionnaires** gagnent si la majorité devine juste.

Au premier procès, le joueur le plus vieux joue le rôle d'un **accapareur** dont c'est le procès. 

L’**accapareur jugé** désigne secrètement d’autres accapareurs parmi les joueurs. Il ne peut pas désigner plus de la moitié des joueurs.

Pour les désigner secrètement, il chuchote "**accapareur**" ou "**révolutionnaire**" à l'oreille de chaque joueur.

Une fois les rôles distribués, l’**accapareur jugé** affirme quelque chose de difficile à deviner mais de facile à vérifier après les votes. Par exemple :

* j’ai un bonbon dans ma poche droite de pantalon

* j’ai le numéro portable de la prof de maths dans mes contacts sur mon téléphone

* j’ai un ami sur Instagram qui a une tête de cheval sur sa photo de profil

* il y a un canard dans le fonds d’écran de mon téléphone

* je porte un caleçon à fleurs

* je viens d’écrire un vrai mot en russe dans le creux de ma main

* il n’y a plus qu’une bouteille de soda au frigo

* j’ai dessiné un cœur dans ce papier

* j’ai plus de 50% de batterie dans mon tel

* j’ai moins de 3 messages whatsapp non lus

* il y a plus de 100 milliards de kilomètre entre la terre et le soleil

* mon cousin croit que la terre est plate

* mon ADN est à 1% typique des habitants du Mali

* Roger est amoureux de moi

L’**accapareur jugé** peut préciser comment les autres **citoyens** pourront vérifier cette affirmation après le vote. L’affirmation est valide si les **citoyens** sont tous d’accord qu’elle sera vérifiable après le vote. 

Tous les **citoyens** doivent deviner la vérité. L’affirmation est-elle vraie ou fausse ? Les **citoyens** vont ensuite voter. Si la majorité a deviné juste, les **révolutionnaires** l’emportent. Si la majorité s’est trompée, les **accapareurs** l’emportent.

Le vote se fait en deux tours. L’**accapareur jugé** ne vote pas.

Le premier tour compte « pour du beurre » et sert à réfléchir et à débattre. Chacun réfléchit d’abord quelques secondes en silence. Chacun met sa main dans son dos. Pour voter « vrai » il faudra montrer sa main poing fermé, pour évoquer la forme d'une brioche. Pour voter « faux » il faudra montrer sa main vide, paume ouverte. Tout le monde dit en chœur et en rythme : « shi-fu-mi ». A « -mi », chacun montre sa main et donc son vote : poing fermé pour vrai ou paume ouverte pour faux. C’est le premier tour.

Ensuite chacun peut exposer ses arguments pour essayer de convaincre les autres ou pour changer d’avis : pourquoi il a voté vrai ou faux. Quand tous ceux qui le souhaitent se sont exprimés, on vote une deuxième fois, « pour de bon ». C’est le deuxième tour.

On compte les votes du deuxième tour pour déterminer le vote majoritaire. En cas d’égalité, le vote « vrai » l’emporte.

L’**accapareur jugé** révèle alors la vérité. Si besoin, les autres **citoyens** vérifient la vérité.

Si la majorité a vu juste, alors les **révolutionnaires** gagnent un point chacun. Si la majorité s’est trompée, l’**accapareur jugé** gagne deux points et les autres **accapareurs** en gagnent un chacun.

Ce **procès révolutionnaire** est terminé. C'est au **citoyen** à la gauche de celui qui vient d'être jugé de tenir le rôle de l'**accapareur** dont c'est le procès.

Le tutoriel s’arrête quand les **citoyens** en ont assez. Le gagnant est le **citoyen** qui a gagné le plus de points.

Ce tutoriel vous a permis de découvrir quelques-uns des principes de ce jeu :

* les **révolutionnaires** veulent faire triompher la vérité pour l'intérêt général,
* les **accapareurs** sont prêts à tromper les autres **citoyens** pour défendre leur propre intérêt,
* les **procès révolutionnaires** servent à faire émerger la vérité sur chaque **citoyen** grâce à des votes,
* tous les **citoyens** peuvent voter et débattre lors de tous les **procès**.

Vous êtes maintenant prêts pour la véritable révolution.

# Matériel nécessaire

* cet ensemble de règles
* une carte mémo par joueur
  * [imprimez-les]({{< ref path="paper-kit.md" lang="en" >}}), 1 carte par joueur
* une pile de **fiches citoyens**
  * imprimez-les à partir de nos [feuilles de calcul Google](https://docs.google.com/spreadsheets/d/1H4vlePftMYvyuFkRnnJuFOS_B0Pv0L4TUgk9htw0TWE/edit?usp=sharing) , au moins 2 fiches par joueur
  * prenez la moitié des fiches et inscrivez **Sans-culotte** (ou **S**) sur la première ligne de leur rabat secret, en bas de fiche
  * prenez l'autre moitié et inscrivez **Privilégié** (ou **P**) sur la première ligne de leur rabat secret, en bas de fiche
* un **Registre du Salut Public**
  * créez une copie de [nos feuilles de calcul Google](https://docs.google.com/spreadsheets/d/1H4vlePftMYvyuFkRnnJuFOS_B0Pv0L4TUgk9htw0TWE/edit?usp=sharing) et jouez avec
* une **forteresse de la Bastille**
  * [imprimez-la](https://siggg.gitlab.io/culottes/img/bastille-map.jpg), 1 seule pour toute la partie
* une pile de **balances de justice** qui se transforment en **bonnets phrygiens** quand elles sont mises à l'envers
  * découpez-les et dėcorez-les à partir d'une [boîte à oeufs en carton]({{< ref path="egg-carton.md" lang="en"  >}}), 1 balance par joueur
* une **pique** par joueur
  * des crayons ou stylos feront office de piques, une pique par joueur
* un sachet de **brioches**
  * vous pouvez utiliser des pois-chiches secs en guise de brioches (ou toute sorte de haricots secs, fruits secs, graines, cailloux ou jetons), 20 par joueur
* un dé à 6 faces

# But du jeu

Votre objectif dépend de qui vous êtes.

Vous êtes soit un **Révolutionnaire** soit un **Accapareur**:

* Si vous êtes un **Révolutionnaire**, vous voulez réaliser l'idéal de la révolution donc vous voulez que les **sans-culottes** reçoivent des **brioches** et vous ne voulez pas que les **privilégiés** en obtiennent.
* Si vous êtes un **Accapareur**, la révolution vous indiffère et vous voulez récupérer un maximum de **brioches** pour vous seul.

Le gagnant est le joueur avec le score le plus élevé à la fin de la partie (voir la rubrique _Comment gagner_ pour plus de détails sur le calcul du score et les stratégies pour gagner).

# Comment démarrer la partie

Les **fiches citoyens** sont mélangées au hasard.

{{< figure src="/culottes/img/06_sans-culotte.jpg" caption="A chaque citoyen sa pique." >}}

Vous, comme tous les autres joueurs :

1. prenez un crayon-**pique**
2. piochez au hasard votre première **fiche citoyen** ; vous n'êtes pas autorisé à révéler ce qui est écrit sur le rabat secret en bas de votre fiche alors pliez-le et rabattez-le sous la fiche ; gardez-le bien cachė jusqu'å la fin de la partie
3. regardez discrètement le contenu du rabat secret du bas de votre **fiche citoyen** et voyez si votre citoyen est un **sans-culotte** ou in **privilégié**
4. choisissez en secret si vous voulez joier cette partie en tant que **Révolutionnaire** \(R) ou en tant qu'**Accapareur** (A) ; inscrivez "R" ou "A" à l'empmacement prévu sur le rabat secret du bas de votre **fiche citoyen** ; veillez à garder cette information secrète jusqu'à la fin de la partie
5. prenez 20 **brioches** et déposez-les sur votre **fiche citoyen**
6. donnez un nom à votre citoyen et inscrivez ce nom à l'emplacement prévu à cet effet sur la **fiche citoyen**

{{< figure src="/culottes/img/07_sans-culotte.jpg" caption="Le style sans-culotte, tendance été 1789." >}}

Regardez maintenant les autres joueurs. Est-ce qu'un joueur porte du bleu, du blanc et du rouge ? Un joueur porte-t-il un long manteau de carmagnole ? ou un **bonnet** ? Qui sait le mieux chanter la Carmagnole ou "ah ca ira !" ? Qui a le plus fort accent français ? Qui a un gilet jaune à portée de main ? Le joueur qui ressemble le plus à un **sans-culotte** commence à jouer en premier. Vous devez tous être d'accord pour que la partie puisse commencer.

# Comment jouer

Votre tour se joue en plusieurs étapes :

## Étape 1 : Utilisez votre citoyen suivant

Si vous avez plusieurs **citoyens**, posez votre crayon-pique sur la fiche du citoyen suivant et jouez-le.
Si vous n'avez qu'une seule fiche de citoyen, vous jouez avec ce citoyen. Posez votre crayon-pique dessus.
* Ce citoyen a-t-il émigré et vous voulez qu'il revienne dans la partie ?
  * Dites simplement _«Vive la révolution»_ et votre citoyen est de retour d'émigration
  * Passez ensuite à l'étape 2: achetez des cocardes.

      {{< figure src="/culottes/img/08_emigration_retour.jpg" caption="Retour d un émigré" >}}

* Voulez-vous passer le tour de ce citoyen?
  * Dites simplement _«Je passe»_ et passez à l’étape 5 : reste-t-il un citoyen. Si le citoyen était émigré, vous n'avez même pas besoin de dire "je passe". Allez directement à l'étape 5.

* Sinon, continuez à l'étape 2 : _acheter des **cocardes**_.

## Etape 2 : acheter des **cocardes**

{{< figure src="/culottes/img/09_cocarde.png" caption="La **cocarde** des **sans-culottes**." >}}

Vous pouvez acheter une **cocarde** si vous le souhaitez

Il suffit de dire _"j'achète une **cocarde**"_.

Acheter des **cocardes** peut vous permettre de prouver ou de prétendre que vous êtes un **sans-culotte**, car leur impact sur votre score final est trois fois plus important pour les **privilégiés** que pour les **sans-culottes**.
Donc, acheter des **cocardes** peut vous aider à vous faire une réputation de véritable sans-culotte.

Vous n'avez rien à payer maintenant. Le prix des **cocardes** sera déduit de votre score final. 1 **cocarde** coûte 1 point de score pour les **sans-culottes**.
1 **cocarde** coûte 3 points pour les **privilégiés**.

## Etape 3 : Donner des **brioches** à la **Bastille**

{{< figure src="/culottes/img/10_bastille_vue.jpg" caption="La réserve stratégique de **brioches** **sans-culottes** de la Bastille." >}}

Vous pouvez donner des **brioches** à la forteresse de **la Bastille** si vous le souhaitez.
Prenez n'importe quel nombre de **brioches** sur votre **fiche de citoyen** actuelle et placez-les dans **la Bastille** pour que les **sans-culottes** (ou les **accapareurs** !) soient bien nourris.

## Etape 4 : **Voter à la brioche** !

Certains **citoyens** sont de véritables **sans-culottes**, tandis que d'autres sont des **privilégiés** qui se sont déguisés en **sans-culottes**.
Peut-être êtes-vous un **accapareur** et voulez-vous que les autres joueurs pensent que vos citoyens sont des **sans-culottes** afin que vous puissiez obtenir des **brioches**.
Le **vote à la brioche** est l'étape la plus importante dans le jeu.

1. Choisissez le **citoyen** pour lequel vous souhaitez voter.
  Il peut s'agir d'une de vos **fiches de citoyen** ou de n'importe quelle **fiche de citoyen** d'un autre joueur.
  {{< figure src="/culottes/img/11_bonnet_louis_capped.jpg" caption="  Le roi mérite-t-il vraiment un **bonnet phrygien** ?" >}}

2. Ce **citoyen** a-t-il une **balance de justice** devant sa fiche de **citoyen** ?
  Si ce n’est pas le cas, prenez une nouvelle **balance de justice** et posez-la devant sa **fiche de citoyen**.
  Si ce **citoyen** a déjà un **bonnet phrygien**, ne prenez pas une nouvelle **balance de justice**, mais retournez le **bonnet phrygien** et il se transforme en **balance de justice**.
  {{< figure src="/culottes/img/12_justice_allegorie.jpg" caption="Apportez-leur une balance de la justice !" >}}
  La **balance de justice** signifie que ce **citoyen** est actuellement en **procès révolutionnaire**.
  Lorsque ce sera son tour, tout joueur pourra **voter à la brioche** en déclarant que ce **citoyen** est un **sans-culotte** ou un **privilégié**.

3. Prenez n'importe quel nombre de **brioches** de votre fiche de **citoyen** et mettez-les sur le plateau **sans-culotte** ou sur le plateau **privilégié** de la **balance de justice** du **citoyen** faisant l'objet de ce **procès révolutionnaire**.

4. Faites noter soigneusement votre **vote à la brioche** en ajoutant une ligne à la fin du **Registre de Salut Public** avec le nom de votre **citoyen* votant, le nom du **citoyen** faisant l'objet du **procès révolutionnaire**, le nombre de **brioches** utilisées pour ce vote et si vous avez voté **sans-culotte** ou **privilégié**.
  Par exemple, _« Robespierre, Danton, 3, privilégié »_ signifierait que votre **citoyen** nommé Robespierre a voté avec 3 brioches pour affirmer que le **citoyen** Danton est un **privilégié**.
  La justice démocratique s'inscrit sur le **Registre de Salut Public**

5. Lancez le **dé révolutionnaire**
  {{< figure src="/culottes/img/13_jeu_de_hasard.jpg" caption="Bonne chance aux **révolutionnaires** et aux **accapareurs** !" >}}

6. Avez-vous obtenu un 5 ou un 6?
  * Si non, alors votre étape de **vote à la brioche** est terminée. Passez à l'*étape 6 : émigrer*.
  * Si vous obtenez un 5 ou un 6, alors votre **vote à la brioche** termine le **procès révolutionnaire** de ce **citoyen** sur lequel vous avez voté, et un verdict doit être rendu et des récompenses doivent être données aux gagnants.

## Etape 5 : Verdict et récompenses

Le **procès révolutionnaire** d'un **citoyen** vient d'être clos. Quel est le verdict ? **Sans-culotte** ou **privilégié** ?

{{< figure src="/culottes/img/14_comite_revolutionnaire.jpg" caption="Verdict du tribunal révolutionnaire" >}}

Retirez la **balance de justice** de ce **citoyen** et comptez le nombre de **brioches** dans chaque plateau de la **balance**.

* S'il y a plus de **brioches** sur le plateau **sans-culotte** que sur le plateau **privilégié**, alors le **citoyen** est considéré comme **sans-culotte**.
  Retournez sa **balance de justice** et elle se transforme en **bonnet phrygien**.
  Grâce à ce **bonnet phyrgien**, ce **citoyen** aura accès aux **brioches** de la **Bastille**.

* S'il y a un nombre égal de **brioches** sur chaque plateau ou bien s'il y a plus de **brioches** sur le plateau **privilégié** que sur le plateau **sans-culotte**, alors le **citoyen** est considéré comme un **privilégié**.
  Il devrait être guillotiné mais, ce jeu étant non violent, vous êtes prié de ne pas décapiter le joueur.
  Et le **citoyen** peut continuer à jouer.

Ce verdict est-il en désaccord avec votre vote ?
Dommage, vous avez perdu les **brioches** avec lesquels vous avez voté du côté des perdants.
Ces **brioches perdantes** doivent être partagés entre les gagnants.

Ce verdict est-il en accord avec votre vote ?
Félicitations, vous avez gagné !
Non seulement vous récupérez vos **brioches gagnantes** (vous récupérez votre mise), mais vous gagnez une partie des **brioches** perdantes.
C'est votre récompense pour avoir bien voté.

{{< figure src="/culottes/img/15_justice_balance.jpg" caption="La balance de justice penche du côté des plus forts, cette fois." >}}

### Combien de brioches perdantes les gagnants obtiennent-ils ?

L'onglet du **Registre de Salut Public** sur nos [feuilles de calcul Google](https://docs.google.com/spreadsheets/d/1H4vlePftMYvyuFkRnnJuFOS_B0Pv0L4TUgk9htw0TWE/edit?usp=sharing) calcule et affiche le nombre de **brioches perdantes** à verser en récompense à la **Bastille** et aux gagnants de ce **vote à la brioche** lorsqu'un **verdict** doit être rendu.

Vous devez indiquer dans le **Registre de Salut Public** que ce procès est clos afin que les **votes à la brioche** correspondants ne soient pas comptés à nouveau lors de futurs **procès**.

Les **brioches** sur le **plateau gagnant** sont appelées des **brioches gagnantes**. Les **brioches** sur le **plateau perdant** sont appelées des **brioches perdantes**. Il y a toujours plus de **brioches gagnantes** que de **brioches perdantes**.

La **Bastille** soutient toujours le **plateau gagnant**. Vous devez faire comme si la **Bastille** avait voté pour le **plateau gagnant** avec autant de **brioches** que la différence entre le nombre de **brioches gagnantes** et le nombre de **brioches perdantes**.

Par exemple, s'il y a 3 **brioches** sur le **plateau** **privilégié** et 1 **brioche** sur le **plateau** **sans-culotte**, alors vous devez faire comme si la **Bastille** avait voté **privilégié** avec 3-1=2 **brioches**.

Les **citoyens** qui ont voté dans ce **procès** récupèrent leurs **brioches gagnantes**. Les **brioches perdantes** sont distribuées entre la **Bastille** et les **citoyens** gagnants proportionnellement à leur nombre de **brioches gagnantes**.
 
Les plus gros gagnants sont récompensés en premier. La **Bastille** est récompensée avant les **citoyens** qui devraient gagner la même proportion de **brioches** qu'elle. Les **citoyens** les plus proches de la gauche du joueur actuel sont récompensés avant les **citoyens** qui devraient gagner la même proportion de **brioches** qu'eux.

Les **brioches de la Bastille** ne peuvent être découpées en morceaux donc les portions de **brioches** à verser en récompense doivent être arrondies au nombre entier le plus proche. Certains gagnants étant récompensés après les autres, ils pourraient ne recevoir aucune **brioche**.

### Exemple

Par exemple, disons que les joueurs A, B et C ont voté **sans-culotte** avec respectivement 1, 2 et 3 **brioches**.
Cela fait 6 **brioches** en faveur du statut **sans-culotte**.
Et les joueurs D et E ont voté **privilégié** avec 2 **brioches** chacun.
Cela fait 4 **brioches** à l'appui des **privilégiés**.

Le **procès** est clos.

Le verdict est **sans-culotte** car 1 + 2 + 3 > 2 + 2.

La différence entre les **brioches** gagnantes et les **brioches** perdues est 1 + 2 + 3 - 2 - 2 = +2.
Nous considérons donc que **la Bastille** a voté **sans-culotte** avec 2 **brioches**.

Le nombre total de brioches gagnants est de 1 + 2 + 3 + 2 = 8.
Il y a 4 **brioches** perdues à répartir entre le joueur A, le joueur B, le joueur C et la forteresse de la **Bastille**, proportionnellement à leurs **brioches** gagnantes :

**La Bastille** devrait recevoir 2 * 4 / 8 = 1 **brioche**.
Le joueur C devrait recevoir 3 * 4 / 8 = 1.5 **brioche**.
Le joueur B devrait avoir 2 * 4 / 8 = 1 **brioche**.
Le joueur A devrait avoir 1 * 4 / 8 = 0.5 **brioche**.

Mais les **brioches** ne peuvent pas être divisées.
Donc, ce nombre est arrondi au nombre de **brioches** le plus proche:

Il y a 4 **brioches perdantes** à répartir.
Le joueur C reçoit 2 **brioches**.
Il reste 2 **brioches perdantes** à répartir.
La Bastille reçoit 1 **brioche**.
Il reste 1 **brioche perdante** à répartir.
Le joueur B reçoit 1 **brioche**.
Il ne reste plus de **brioche perdante** à répartir.
Le joueur A n'en reçoit aucune.

{{< figure src="/culottes/img/16_comite_revolutionnaire.jpg" caption="Fin du procès." >}}

## Etape 6 : Emigrer

Votre **citoyen** pourrait vouloir quitter la partie, soit temporairement, soit définitivement.

{{< figure src="/culottes/img/17_emigration_depart.jpg" caption="Départ d'émigrés privilégiés." >}}

Il suffit de dire _« j'émigre »_.

_Rappel :_ les prochains tours de ce citoyen se feront comme si vous disiez «Je passe» pour ce citoyen (voir l’étape 1) jusqu’à ce que vous disiez _« Vive la révolution ! »_ À l’étape 1 ou jusqu’à la fin du jeu.

## Etape 7 : Reste-t-il un citoyen ?

Si vous avez déjà plusieurs **fiches de citoyen**, vous jouez votre prochain citoyen en passant à l'étape 1: _choisissez votre prochain citoyen_.

Une fois que vous avez joué toutes vos **fiches de citoyen**, passez à l’étape 8: recruter un nouveau citoyen.

## Etape 8 : recruter un nouveau citoyen

Plus vous avez de **citoyens** en jeu plus vous avez d'influence dans la partie. Mais recruter un nouveau **ciyoyen** coûte plus cher pour les **révolutionnaires** que pour les **accapareurs**. Voulez-vous recruter un nouveau **citoyen** ?

Il suffit de dire _« je recrute un nouveau citoyen »_.

{{< figure src="/culottes/img/18_sans-culotte.jpg" caption="Nouvelle recrue **sans-culotte**" >}}

Piochez au hasard une **fiche de citoyen** supplémentaire.

* Si vous êtes un révolutionnaire, écrivez secrètement «R» (comme **révolutionnaire**) sur le rabat inférieur secret de la feuille et voyez si votre nouveau citoyen est un **sans-culotte** ou un **privilégié**. Vous n'êtes pas autorisé à montrer ce volet à d'autres joueurs. Donnez un nom à ce nouveau citoyen et écrivez-le sur sa **fiche**.

* Si vous êtes un **accapareur**, la révolution ne vous intéresse pas sauf si elle vous permet d’obtenir plus de **brioches**. Mais vous pouvez faire semblant d'être un **révolutionnaire** et recruter de faux **citoyens** (des _hommes de paille_) que vous utiliserez comme des marionnettes. Il suffit de dire _ «je recrute un nouveau citoyen »_. Choisissez au hasard une **fiche de citoyen** supplémentaire. Écrivez secrètement «A» (comme dans **Accapareur**) sur le rabat inférieur de la feuille et faites semblant de lire si votre nouveau **citoyen** est un **sans-culotte** ou un **privilégié**. En réalité, puisque vous êtes un **accapareur**, tous vos **citoyens** sont du même type : **sans-culottes** si votre première **fiche de citoyen** indique **sans-culotte** ou  **privilégiés** si votre première **fiche de citoyen** indique **privilégié**. Ce sont de fausses identités pour votre premier et seul **citoyen**. La mention sur les feuilles supplémentaires doit donc être ignorée. Donnez un nom à ce faux **citoyen** et écrivez-le sur la feuille.)

## Etape 9 : Distribuer des brioches de la Bastille

Regardez chaque **citoyen**, en commençant par le premier **citoyen** du joueur à votre gauche.

{{< figure src="/culottes/img/20_solidarite_repas.jpg" caption="Un dîner est offert par un **privilégié** à la foule en colère de **sans-culottes**." >}}

Ce **citoyen** a-t-il un **bonnet phrygien** (et non une **balance de justice** qui indiquerait que son **procès révolutionnaire** est en courd) ? Sinon, examinez le **citoyen** suivant jusqu'à ce que vous ayez examiné tous les **citoyens**.

Si un **citoyen** a un **bonnet phrygien**, prenez une **brioche de la Bastille** et mettez-la sur sa **fiche de citoyen**. Inscrivez +1 au nombre de **brioches de la Bastille** distribuées à ce citoyen.

S'il ne reste plus de **brioche** dans la **Bastille**, alors… tant pis pour les prochains **citoyens**.

{{< figure src="/culottes/img/21_solidarite_grain.jpg" caption="Le grain est redistribué aux plus nécessiteux" >}}

## Etape 10 : Fin de tour

Votre tour est maintenant terminé. Le joueur à votre gauche joue son tour.

# Fin de la partie

La partie se termine:

* quand tous les **citoyens** ont **émigré** 

* ou une fois que tous les joueurs sont d’accord pour arrêter la partie et que chaque **citoyen** a joué un dernier tour

{{< figure src="/culottes/img/22_jeu_cartes.jpg" caption="Le **privilégié** ou le **sans-culotte** ? Le **révolutionnaire** ou l’ **acccapareur**? Qui gagne ?" >}}

# Comment gagner

Le gagnant est le joueur avec le score le plus élevé à la fin de la partie.

Pour gagner, il faut donc savoir le calculer, bien le comprendre et employer des stratégies créatives d'attaque ou de défense lors des **votes à la brioche**.

## Calculez votre score

A la fin de la partie, remplissez votre **fiche de citoyen** pour calculer votre score final :

1. Vous, et tous les joueurs, révélez la vraie nature de vos citoyens en dépliant le rabat  secret en bas de leur **fiche citoyen** : **révolutionnaire** ou **accapareur** ? **sans-culotte** ou **privilégié** ?

2. pour chaque **accapareur** : combien de **brioches** possédent leurs **citoyens** au total à la fin du jeu ? soustrayez 20 par **citoyen** (leur nombre initial de **brioches**) à ce totan : c’est le nombre de **brioches** que cet **accapareur** a gagné pendant la partie. Ajoutez ce nombre au score de cet **accapareur**.

3. pour les **révolutionnaires** : combien de **brioches** ont été distribuées de la **Bastille** aux **sans-culottes** ? ajoutez ce nombre au score de chaque **révolutionnaire**.

4. combien de **brioches** ont été distribués de la **Bastille** aux **privilégiés** ? soustrayez ce nombre du score de chaque **révolutionnaire**.

5. pour chaque **révolutionnaire** : combien de **citoyens** avez-vous recrutés ? soustraire ce nombre de votre score.

6. pour chaque **sans-culotte** : combien de **cocardes** avez-vous acheté ? Soustraire ce nombre de votre score.

7. pour chaque **privilégié** : combien de **cocardes** ce **privilégié** a-t-il acheté? multiplier ce nombre par 3 puis soustraire le résultat de votre score.

8. pour chaque joueur : additionner le score de vos **citoyens** si vous êtes un **accapareur** ou choisissez le meilleur score de vos **citoyens** si vous êtes un **révolutionnaire**

{{< figure src="/culottes/img/23_pain_a_Versailles.jpg" caption="'* Nous voulons des **brioches** et non des lois ni des contrats intelligents !'" >}}

## Bien comprendre le score

Qui vous êtes | Diminue votre score | Améliore votre score
:--- | :--- | ---:
**Accapareur** | Vous donnez des  **brioches** à la  **Bastille** (-1 point par brioche) | La **Bastille** vous distribue des **brioches**  (+1 point par brioche)
**Accapareur** | Vous perdez des **votes à la brioche**  (-1 point par brioche) | Vous gagnez des **votes a la brioche**  (+1 point par brioche)
**Accapareur** ou **Révolutionnaire** | Vous achetez des **cocardes** (-1 point par cocarde si vous êtes **sans-culotte**, -3 point par cocarde si vous êtes **privilégié**) |
 **Révolutionnaire** | La **Bastille** distribue des **brioches** à des **privilégiés**  (-1 point par brioche) | La **Bastille** distribue des **brioches** à des **sans-culottes**  (+1 point par brioche)
**Révolutionnaire** | Vous recrutez un **citoyen** (-1 point par citoyen recruté) |

Il y a donc quatre cas. Vous pouvez être :

* un **révolutionnaire sans-culotte**, auquel cas vous voudrez peut-être prouver que vous êtes **sans-culotte** et savoir qui est un **sans-culotte** et qui est **privilégié** pour contribuer au mieux à la **révolution**

* un **révolutionnaire privilégié**, vous ne voudrez probablement pas que les autres croient que vous êtes un **sans-culotte** et vous voudrez peut-être savoir qui est un **sans-culotte** et qui est un **privilégié** pour pouvoir contribuer au mieux à la **révolution**

* un **accapareur sans-culotte**, auquel cas vous pourriez vouloir prouver que vous êtes un **sans-culotte** et vous voudrez peut-être faire semblant d'être un **révolutionnaire** pour pouvoir influencer les autres joueurs, mais vous ne vous souciez probablement des autres que dans la mesure où cela vous aide à obtenir plus de **brioches**,

* un **accapareur privilégié**, alors vous pourriez vouloir faire semblant que vous êtes un **revolutionnaire sans-culotte** pour amener les autres à vous laisser prendre plus de **brioches** et vous ne vous souciez des autres que dans la mesure où cela vous permet d'obtenir plus de **brioches**.

{{< figure src="/culottes/img/24_inegalites.jpg" caption="Les inégalités ne dureront pas !" >}}

Dit autrement, votre score dépend principalement de si vous êtes un **révolutionnaire** ou un **accapareur** :

* Si vous êtes un **révolutionnaire**, plus le nombre de **brioches** distribuées aux **sans-culottes** sera élevé, plus votre score sera élevé ; et plus il y aura de **brioches** distribuées aux **privilégiés**, plus votre score sera bas.

* Si vous êtes un **accapareur**, plus votre obtiendrez de **brioches** au cours de la partie, plus votre score sera élevé.

{{< figure src="/culottes/img/25_inegalites_renversement.jpg" caption="*L'égalité* enfin ?" >}}

Votre score dépend également du nombre de **cocardes** que vous achetez pendant le jeu afin d'être reconnu comme un véritable **sans-culotte** et d'avoir accès aux **brioches de la Bastille**. Plus vous achetez de **cocardes**, plus votre score baisse. Les **cocardes** sont 3 fois plus chères pour les **privilégiés** que pour les véritables **sans-culottes**. Le score des **privilégiés** diminuera donc encore plus avec le nombre de **cocardes** qu'ils achètent pour se faire passer pour des **sans-culottes**.

{{< figure src="/culottes/img/26_comite_revolutionnaire.jpg" caption="La révolution doit réussir ! Ou pas." >}}

Votre score dépend enfin du nombre de **citoyens** que vous recrutez. Si vous êtes un **révolutionnaire**, recruter de nouveaux **révolutionnaires** est difficile car il faut les convaincre et votre score sera affecté par le nombre de **citoyens** que vous avez recrutés. Mais si vous êtes un **accapareur**, magouiller et corrompre sont votre quotidien et vous pouvez gratuitement recruter des **citoyens** _"hommes de paille"_ et les faire passer pour des **révolutionnaires** alors qu'ils ne servent que vos propres intérêts. Votre score ne sera pas affecté par le nombre de **citoyens** que vous recrutez.

{{< figure src="/culottes/img/99_bastille.jpg" caption="A l'assaut de la **Bastille** !" >}}

## Stratégie d'attaque par corruption

**Les révolutionnaires** comme les **accapareurs** peuvent essayer de remporter des **votes à la brioche** grâce a des **attaques par corruption**. Il vous suffit de faire une annonce, à tout moment dans la partie, telle que celle-ci :

{{< blockquote >}}
Si vous votez comme moi dans ce procès, je vous promets que je vous rembourserai les brioches éventuellement perdues et que je vous offrirai même une brioche en plus, en consolation, en cas de perte.
{{</blockquote >}}

Vous pouvez imaginer et tenter des promesses simimaires ou inspirées de celle-ci. Vous pouvez également demander à une personne de confiance (même à un non-joueur) de prendre quelques unes de vos brioches en caution et de les utiliser pour mettre votre promesse à exécution.

On appelle également ce type de promesse des _attaques p+epsilon_.

## Stratégie de défense par alliance

Vous pouvez tenter de vous défendre contre les **attaques par corruption** en formant des **alliances de défense**. Vous pouvez proposer une alliance de ce type à tout moment. Par exemple :

{{< blockquote >}}
Si vous rejoignez mon alliance contre ce **citoyen**, vous devez mettre une **brioche** en caution. Elle vous sera remboursée si vous votez bien contre ce **citoyen**. Mais si vous trahissez l'alliance en votant pour ce **citoyen**, alors votre caution sera distribuée aux membres fidèles de l'alliance.
{{</ blockquote >}}

Vous pouvez demander à une personne de confiance de gérer votre alliance en conservant et en distribuant les cautions comme promis.

## Autres stratégies

L'objectif de ce jeu est d'identifier des stratégies gagnantes pour les **accapareurs** et pour les **révolutionnaires**. Merci de nous laisser un commentaire ci-dessous pour nous raconter comment vous avez réussi a gagner une partie !

# Comment contribuer

Vous pouvez contribuer à améliorer ce jeu.

Pour plus de détails, lisez [la page *Contribute* (en anglais)]({{< ref path="contribute.md" lang="en" >}}).